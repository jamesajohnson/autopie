import { Component, OnInit } from '@angular/core';

import { DeviceService } from './services/device.service';

@Component({
    selector: 'my-app',
    templateUrl: './app/app.component.html',
    providers: [ DeviceService ]
})
export class AppComponent implements OnInit {

    constructor(
        private deviceService: DeviceService
    ) { }

    ngOnInit() {
        
    }
}
