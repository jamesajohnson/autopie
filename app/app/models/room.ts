import { Device } from '/device.ts';

export class Room {
    roomName: string;
    devices: Device[];
}
