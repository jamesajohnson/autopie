export class Device {
    deviceId: string
    roomId: string
    deviceName: string
    port: string
}
