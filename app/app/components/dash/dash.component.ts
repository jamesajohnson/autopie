import { Component, ViewChild, OnInit } from '@angular/core';

import { DeviceService } from '../../services/device.service';

import { Device } from '../../models/device';
import { Room } from '../../models/room';

@Component({
    selector: 'app-dash',
    templateUrl: './app/components/dash/dash.component.html',
    styleUrls: ['./app/components/dash/dash.component.css']
})
export class DashComponent implements OnInit {

    rooms: Room[] = [];
    roomsLoaded: boolean = false;


    editingDeviceIndex: number;
    editingRoomIndex: number;
    editing: boolean = false;

    constructor(
        private deviceService: DeviceService
    ) { }

    ngOnInit() {
        this.deviceService.getRooms().then(rooms => {
            console.log(rooms);
            this.rooms = rooms;
            this.roomsLoaded = true;
        });
    }

    /**
    * Return a device by its room and device index
    *
    * @param     roomIndex     number     The index of the devices room
    * @param     deviceIndex   number     The index of the device
    * @return    Device        The device requested
    */
    getDeviceByIndexes(roomIndex: number, deviceIndex: number): Device {
        if (this.roomsLoaded) {
            return this.rooms[0].devices[0];
        } else {
            return {deviceName:'', port:''} as Device;
        }
    }

    /**
    * Return a room by its index
    *
    * @param     roomIndex     number     The index of the devices room
    * @return    Room          The room as position [index]
    */
    getRoomAtIndex(roomIndex: number): Room {
        if (this.roomsLoaded) {
            return this.rooms[0]['room'];
        } else {
            return {roomName:''} as Room;
        }
    }

    /**
    * Send request to switch a devices GPIO state
    *
    * @param      room      string      ID of the room the device is in
    * @param      port      string      GPIO port of the device
    * @param      client    string      Client IP address
    */
    switchDevice(room: string, port: string, client: string){
        this.deviceService.switchDevice(room, port)
        .then(response => {
            console.log(response);
        });
    }

    /**
    * Mark a room as being edited
    * @param     roomIndex     number     The index of the room being edited
    */
    editRoom(roomIndex: number): void {
        this.editing = !this.editing;
        this.editingRoomIndex = roomIndex;
    }

    /**
    * Returns whether a room is being edited
    * @param     roomIndex     number     The ID of the room to check
    * @return    boolena       If the room is being edited
    */
    isEditingRoom(roomIndex: number): boolean {
        return this.editing && roomIndex === this.editingRoomIndex;
    }

    /**
    * Display the device edit dialog for a device
    *
    * @param     deviceIndex     number     The index of the device to edit
    * @param     roomIndex       number     The index of the room the device belongs to
    */
    editDevice(deviceIndex: number, roomIndex: number) {
        this.editingDeviceIndex = deviceIndex;
        this.editingRoomIndex = roomIndex;
    }

    /**
    * Update Device with new params
    *
    * @param     device     Device     The device to update
    */
    updateDevice(device: Device) {
        this.deviceService.updateDevice(device);
    }

    /**
    * Update Room with new params
    *
    * @param     room     Room     The room to update
    */
    updateRoom(room: Room) {
        this.deviceService.updateRoom(room);
    }
}
