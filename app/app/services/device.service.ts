import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'

import { Device } from '../models/device';
import { Room } from '../models/room';

@Injectable()
export class DeviceService {

    rest = '192.168.1.4';

    constructor(
        public http: Http
    ) { }

    getRooms(client: string = this.rest): Promise<Object[]> {
        return this.http
        .get("http://" + client + ":3000/api/rooms/")
        .toPromise()
        .then(data => data.json() as Room[]);
    }

    switchDevice(room: string, port: string, client: string = this.rest): Promise<Object> {
        return this.http
        .get(`http://${client}:3000/api/rooms/${room}/devices/${port}/switch`)
        .toPromise()
        .then(data => data.json() as Object[]);
    }

    getDeviceStatus(room: string, port: string, client: string = this.rest): Promise<Object> {
        return this.http
        .get(`http://${client}:3000/api/rooms/${room}/devices/${port}/status`)
        .toPromise();
    }

    updateDevice(device: Device, client: string = this.rest): void {
        console.log('Put Device');
        console.log(device);
        this.http
        .put(`http://${client}:3000/api/rooms/${device.roomId}/devices/${device.deviceId}`, device);
    }

    updateRoom(room: Room, client: string = this.rest): void {
        console.log('Put Room');
        console.log(room);
        this.http
        .put(`http://${client}:3000/api/rooms/${room.roomId}`, room);
    }
}
