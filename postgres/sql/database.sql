/*
* Create the room table.
*/

DROP TABLE IF EXISTS room;

CREATE TABLE room (
    roomId SERIAL PRIMARY KEY,
    roomName VARCHAR(50),
    roomDescription VARCHAR(500)
);

/**
* Create device table.
*/

DROP TABLE IF EXISTS device;

CREATE TABLE device (
  deviceId SERIAL PRIMARY KEY,
  roomId INT,
  port INT,
  deviceName VARCHAR(50),
  deviceDescription VARCHAR(500)
);

-- Create test data
INSERT INTO room (roomId, roomName, roomDescription) VALUES
(0, 'Living Room','The second best room in the house');

INSERT INTO device (deviceId, roomId, port, deviceName, deviceDescription) VALUES
(0, 0, 1, 'Lamp','Owl lamp');
