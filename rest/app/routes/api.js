const pg = require('pg');
const express = require('express');
const router = express.Router({mergeParams: true});

const conString = 'postgres://postgres:password@postgresql/test';

/**
* Middleware for every request.
*/
router.use(function(req, res, next) {
    next();
})

router.get('/', (req, res) => {
    res.send(`
        e Y8b                 d8               888 88e  ,e,
       d8b Y8b    8888 8888  d88    e88 88e    888 888D  "   ,e e,
      d888b Y8b   8888 8888 d88888 d888 888b   888 88"  888 d88 88b
     d888888888b  Y888 888P  888   Y888 888P   888      888 888   ,
    d8888888b Y8b  "88 88"   888    "88 88"    888      888  "YeeP"
    `);
})

// routes
router.use('/rooms', require('./rooms'));
router.use('/rooms/:roomId/devices', require('./devices'));

module.exports = router;
