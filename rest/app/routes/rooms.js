const express = require('express');
const router = express.Router({mergeParams: true});
const pg = require('pg');

const conString = 'postgres://postgres:password@postgresql/test';

router.route('/')
    .post(function(req, res) {

        const room = req.body;

        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err);
                return next(err);
            }
            client.query(`
            INSERT INTO room (roomName, roomDescription)
            VALUES ($1, $2);`, [room.roomName, room.roomDescription], function (err, result) {
                done();

                if (err) {
                    return next(err);
                }

                res.sendStatus(200);
            });
        });
    })
    .get(function(req, res) {
        pg.connect(conString, function (err, client, done, next) {
            if (err) {
                console.log(err);
                return next(err);
            }
            //get all devices first
            client.query(`SELECT * FROM device`, [], function (err, result) {
                    var devices = result.rows;

                    // get all rooms
                    client.query(`SELECT * FROM room`, [], function (err, result) {
                        done();

                        if (err) {
                            console.log(err);
                            return next(err);
                        }

                        // match devices to rooms
                        let results = [];

                        for (let room of result.rows) {
                            roomDevices = devices.filter(device => {
                                return device.roomid === room.roomid;
                            });

                            results.push({
                                room: room,
                                devices: roomDevices
                            });
                        }

                        res.json(results);
                    });
            });
        });
    }
);

router.route('/:roomId')
    .get(function(req, res) {
        pg.connect(conString, function (err, client, done, next) {
            if (err) {
                console.log(err);
                return next(err);
            }
            //get all devices first
            client.query(`
                SELECT * FROM device
                WHERE roomId = ${req.params.roomId}`, [], function (err, result) {
                    var devices = result.rows;

                    // get all rooms
                    client.query(`
                        SELECT * FROM room
                        WHERE roomId = ${req.params.roomId}`, [], function (err, result) {
                        done();

                        if (err) {
                            console.log(err);
                            return next(err);
                        }

                        // match devices to rooms
                        let results = [];

                        for (let room of result.rows) {
                            roomDevices = devices.filter(device => {
                                return device.roomid === room.roomid;
                            });

                            results.push({
                                room: room,
                                devices: roomDevices
                            });
                        }

                        res.json(results);
                    });
            });
        });
    })
    .put(function(req, res) {

    })
    .delete(function(req, res) {
        pg.connect(conString, function (err, client, done, next) {
            if (err) {
                console.log(err);
                return next(err);
            }
            client.query(`
                DELETE FROM room
                WHERE roomId = ${eq.params.roomId}`, [], function (err, result) {
                done();

                if (err) {
                    console.log(err);
                    return next(err);
                }

                res.sendStatus(200);
            });
        });
    }
);

module.exports = router;
